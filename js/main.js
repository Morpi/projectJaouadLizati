var swiper = new Swiper('.slider-color', {
	initialSlide: 2,
	slidesPerView: 5,
	centeredSlides: true,
});
var swiper = new Swiper('.slider-buttons', {
	initialSlide: 1,
	slidesPerView: 2,
	centeredSlides: true,
	breakpoints: {
		768: {
			slidesPerView: 3,
		}
	}
});
var swiper = new Swiper('.slider-icons', {
	initialSlide: 3,
	slidesPerView: 6,
	centeredSlides: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});

$(document).ready(function() {
	$('.header__toggler').on('click', function() {
		$(this).toggleClass('close')
		$('.header__menu').toggleClass('show')
	})
})

